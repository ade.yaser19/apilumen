<?php

namespace App\Http\Controllers;

use App\Models\Mst_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
   public function register(Request $request)
   {
    $this->validate($request,[
      'name' =>'required',
      'email'=>'required|email|unique:mst_user',
      'password'=>'required',
    ]);

    $user = Mst_user::create([
      'name' => $request->name,
      'email'=> $request->email,
      'password'=>Hash::make($request->password),
      'api_token'=> Str::random(50)
    ]);

    return response()->json(['status'=>'success','data'=>$user],200);

   }

   public function login(Request $request)
   {
      $this->validate($request,[
         'email'=>'required',
         'password'=>'required'
      ]);

      $user = Mst_user::where('email',$request->email)->first();

      if($user && Hash::check($request->password, $user->password)){
         $user->update(['api_token'=>Str::random(50)]);
         return response()->json(['status'=>'success','api_token'=>$user->api_token],200);
      }

      return response()->json(['status'=>'failed','message'=>'Email atau Password Salah!'],400);
   }
}
