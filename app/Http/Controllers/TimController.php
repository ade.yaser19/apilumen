<?php

namespace App\Http\Controllers;

use App\Models\Mst_tim;
use Illuminate\Http\Request;

class TimController extends Controller
{
    public function index(){

        $data = Mst_tim::orderBy('created_at','DESC')->get();
        return response()->json(['status'=>'success','messages'=>'successfully fetched data','data'=>$data], 200);
    }

    public function create(Request $request){

        $this->validate($request,[
            'name'=>'required',
            'jabatan'=>'required',
            'layer' =>'required'
            ],[
            'name.required'   =>'Nama harus di isi',
            'jabatan.required'=>'Jabatan harus di isi',
            'layer.required'  =>'Jabatan harus di isi'
            ]
        );

        $data = Mst_tim::create([
            'name'   =>$request->name,
            'jabatan'=>$request->jabatan,
            'layer'  =>$request->layer
        ]);

        return response()->json(['status'=>'success','message'=>'Data Berhasil Disimpan!','response'=>$data], 200);
    }

    public function update(Request $request,$id){
        $data = Mst_tim::find($id);
        $data->update($request->all());
        return response()->json(['status'=>'success','message'=>'Data Berhasil Disimpan!','response'=>$data], 200);
    }

    public function delete($id){
        $data = Mst_Tim::find($id);
        $data->delete();
        return response()->json(['status'=>'success','message'=>'Data Berhasil Dihapus!','response'=>$data], 200);
    }
}
