<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Mst_Tim extends Model {
    protected $table = 'mst_tim';
    protected $fillable =['name','jabatan','layer'];
}


