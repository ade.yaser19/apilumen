<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/register','AuthController@register');
$router->post('/login','AuthController@login');

$router->group(['middleware'=>'auth'], function() use($router){
    $router->get('/tim','TimController@index');
    $router->post('/tim/create','TimController@create');
    $router->put('/tim/update/{id}','TimController@update');
    $router->delete('/tim/delete/{id}','TimController@delete');
});